<?php
//PASSING BY REFERENCE

function tambah_string(&$str){
    $str = $str . ", Amalya";
    return $str;
}

$string = "Windy Sayyida Amalya ";
echo "\$string = $string<br>";
echo tambah_string($string). "<br>";
echo "\$string = $string<br>";

?>